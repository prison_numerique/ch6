---
title: "#1  Projet"
draft: false
tags:
---
## Contexte
Pour situer le projet, et donc envisager les moyens qui ont été mis en oeuvre pour ce projet porté entre 2013 et 2022, nous proposons quelques éléments de contexte.
### Un programme IDEFI
Le programme IDEFI, lancé par l’Agence Nationale de la Recherche en 2012, représente une initiative d'avant-garde dans le paysage éducatif français. Conçu pour encourager l'excellence et l'innovation dans l'enseignement supérieur, IDEFI se distingue par son approche originale visant à transformer les méthodes pédagogiques et à améliorer la qualité de la formation universitaire.

À travers un appel à projets, l’ANR a invité les universités et institutions académiques à proposer des dispositifs pédagogiques novateurs sous la bannière des Initiatives D’Excellence en Formations Innovantes (IDEFI). Le but était de créer un environnement où l'expérimentation pédagogique pourrait prospérer, favorisant ainsi une approche plus dynamique et réactive à l'enseignement.

### Notre projet IDEFI
Notre université, en collaboration étroite avec la Faculté des Sciences de l’Université de Montpellier et l’Université de Nîmes, a répondu à cet appel en mettant en place un projet particulièrement ambitieux axé sur le soutien à la réussite des étudiants en première année de licence. Ce projet s'est distingué non seulement par son focus sur une étape cruciale de la vie universitaire mais aussi par son mode de conception collaboratif. En effet, le programme a été élaboré conjointement avec les enseignants, les départements académiques et les services administratifs de l'université, qui ont tous eu l’opportunité de soumettre leurs idées via un appel à projet interne.

L'organisation de l’IDEFI-UM3D, notre projet spécifique au sein de ce programme, est à l'image d'une grande orchestration où plus d’une vingtaine d’actions sont réparties sur les trois établissements partenaires. Le chargé de mission IDEFI joue ici un rôle crucial, agissant en tant que chef d'orchestre pour maintenir la cohésion et l'efficacité du projet. Il est responsable du pilotage global, tant sur le plan budgétaire qu'administratif, et s'assure de la synergie entre les différentes actions. Pour éviter que le projet ne se dilue en une multitude d'initiatives isolées, des réunions du "Groupe projet" sont organisées semi-annuellement, offrant une plateforme d'échange et de partage entre les responsables des différentes actions.

![[idefi-screen-upv-3-axes.png]]
*Les 3 axes de ce projet IDEFI - [Lien vers la page](https://www.univ-montp3.fr/fr/formation-lp/idefi-um3d/les-actions-idefi-um3d) - [Plaquette PDF](./media/idefi-um3d-plaquette_0.pdf)*

En outre, un "Groupe de recherche interdisciplinaire" enrichit régulièrement le programme en proposant des conférences et des journées thématiques axées sur la transformation pédagogique et une meilleure compréhension des besoins des étudiants. Ces initiatives soulignent l'engagement du programme à favoriser une réflexion continue et un dialogue enrichissant entre tous les acteurs impliqués.

## Périmètre
### Publics empêchés dans IDEFI UM3D
Au sein de projet UM3D, la pédagogie à destination des **publics empêchés** fait partie intégrante de l'axe "Diversification des parcours adaptés", au sein de l'action 13.

Le public bénéficiaire de l'action est inscrit en tant qu'étudiant en première année de licence, depuis un établissement pénitentiaire.
### Objectif de l'action
Par la formation des RLE et le soutien des enseignants en université, la mise à disposition de modules "offlines"[^offline] pour les étudiants-détenus.
[^offline]: Modules qui puissent fonctionner comme un site "web" sans l'accès à internet, ce qui suppose que toutes les ressources soient intégrées sans *appeler* de contenu dynamique au sein du world wide web (cf [la revue technopédagogique du chapitre 2](https://prison_numerique.gitpages.huma-num.fr/ch2))

Par-delà les besoins au sein des établissements participants à IDEFI-UM3D, des livrables ont été confectionnés, afin de promulguer de bonnes pratiques et faciliter le travail des porteurs de projets et enseignants pour public-empêchés (RLE ou enseignants au sein des OF/établissements partenaires).

Retrouvez l'ensemble des livrables sur [la page dédiée aux ressources de ce projet](./01-projets/idefi/02-docs).
### Moyens
Pour mener à bien ce travail, 700HETD ont été financés par les partenaires, et 230HETD par l'Université Paul-Valéry. Les enseignants chercheurs et doctorants affectés à l'action 13 étaient tous rattachés à l'unité de recherche LHUMAIN.

## Acteur clés
### Au sein des universités impliquées
Le projet IDEFI UM3D impliquait plusieurs universités, tant sur le plan pédagogique qu'expérimentale. Voici la description du rôle de l'Université Paul-Valéry Montpellier 3 dans ce projet. Cela illustre la structuration d'un tel projet dans une université. Les autres universités ont appliqué un motif d'implication semblable.
Ainsi, le rôle de notre université a été de soutenir et coordonner les actions pédagogiques et administratives en mettant à disposition moyens humains et financiers et parmi eux :
1. Le responsable de la Direction des Études et de la Scolarité (DES) qui facilite le processus d'inscription des étudiants-détenus. Dans le cadre de ce projet, il a pu porter attention à ce type de dossiers.
2. Le responsable pédagogique de la formation universitaire à distance à laquelle le futur étudiant-détenu est inscrit. Celui-ci relaie l'information auprès de l'ensemble de l'équipe pédagogique sur les conditions particulières d'apprentissage. Il  récupère les supports de cours sous leur format numérique et indique si des adaptations sont nécessaires.
3. Si des adaptations sont nécessaires, l'ingénieur pédagogique et l'enseignant responsable de l'unité d'enseignement travaillent en binôme. L'ingénieur pédagogique peut outiller le support pour le rendre accessible et l'enseignant responsable, peut revoir des formulations de consignes ou la présence de liens hypertextes voire aménager des modalités de contrôle de connaissances avec l'aval de la Direction des Études et de la Scolarité.  
4. La FIED recense et relaie les offres de formation universitaire en enseignement à distance proposées sur l'ensemble du territoire nationale. La FIED restitue cette offre notamment pour les personnes détenues et y ajoute une personne référente du site universitaire qui relaie les demandes d'inscriptions aux équipes pédagogiques et à la DES. Dans le cadre de ce projet, la FIED a donc permis un rayonnement national.
### Partenaires
![[idefi-screen-partenaires.png]]
Maisons centrales de Poissy et Réau (France) et Benevento (Italie) : Ont permis l'expérimentation avec la participation des proviseurs de lycées pénitentiaires et des RLE.

La Conférence des Présidents d'Université a principalement participé, avec la FIED, à la constitution des deux premier guides présents dans les [[01-projets/idefi/02-docs|ressources]]. L'EPEA a permis de lier les équipes francophones et les équipes Italiennes. La Chaire UNESCO de recherche appliquée pour l'éducation en prison a participé a bien des égards au projet, apportant visibilité avec leur publication au travail réalisé, étant présents pour des séminaires et par leur réseau d'acteurs et de ressources.

Eramsus+ a financé une partie du projet [[01-projets/spoc-in-prison/01-projet|SPOC in prison]], lui même partie intégrante d'IDEFI UM3D. 

## IDEFI UM3D et SPOC in prison, quelle articulation ?
Le premier instaure un cadre général, institutionnel et des procédures administratives et pédagogiques quand le second déploie des parcours d'apprentissage numérique à distance et des expérimentations sur le terrain carcéral. 

Les retombées des deux projets amènent aujourd'hui à considérer un processus de pédagogie numérique courte adapté au milieu carcéral portant sur des activités d'études supérieures ou bien des activités d'éducation informelle. Pour les activités scolaires qui suivent le programme national, de nombreuses ressources existent mais sont crées et développées par les enseignants directement à partir de plateformes internes non disponibles au public mais existantes. C'est pourquoi il convient de demander systématiquement aux enseignants avec qui vous travaillez en prison, les outils dont ils disposent pour enseigner avec le numérique.