---
title: "#2  Ressources"
draft: false
tags:
---
Les livrables de l'action 13 IDEFI UM3D étaient mentionnés dans les objectifs du projet. En voici la liste : 
1. Guide administratif : *à destination personnels universitaires et la direction de l'administration pénitentiaire pour faciliter les démarches d'inscription et de scolarité des candidats ou étudiants détenus.*
2. Guide pédagogique : *à destination des enseignants-chercheurs et tout formateur qui souhaite proposer un parcours pédagogique adapté aux contraintes de l'enseignement à distance sans accès à internet, en prison.*
3. Guide ethnographique : *un webdocumentaire qui retrace l'expérience de tous les acteurs impliqués dans le projet, qu'il s'agisse d'enseignants, concepteurs, détenus.*
Ces trois ressources sont le produit de 9 années au sein du programme IDEFI UM3D, dont les bases avaient été posées entre 2010 et 2013 au cours de la thèse de L. Alidières.
## Guide de scolarité
[Accéder à la ressource](./media/idefi-um3d-guide-admin.pdf)

Public cible : 
- Responsable local d'enseignement / Conseilleur pénitentiaire d'insertion et de probation
- Direction de l'Administration pénitentiaire
- Personnels universitaires

Description : 
Avec l'aide de la FIED[^fied], nous avons mené un groupe de travail pour : 
- expliquer l’ensemble des démarches administratives aux Responsables Locaux d’Enseignement (RLE) ; 
- faciliter la mise en place des procédures administratives, organisationnelles et techniques correspondantes dans les établissements d’enseignement supérieur français.

[^fied] Fédération Interuniversitaire de l'enseignement à distance - [Site web](https://www.fied.fr)
## Guide pédagogique
[Accéder à la ressource](./media/idefi-um3d-guide-peda.pdf)

Public cible :
- Enseignants dans le supérieur ou d'autres formations[^disclaimer1]
- Formateurs
- Ingénieurs pédagogiques ou médiateurs numériques
[^disclaimer1] Et tous les enseignants finalement, car l'expérimentation était menée en Université, mais finalement, la plupart des mesures proposées peuvent convenir à de nombreuses formations telles que "Français langue étrangère", ou des formations professionnelles, technologiques.

Description :
Ce guide promulgue de bonnes pratiques pédagogiques en matière de scénarisation et médiatisation pédagogique pour les enseignant·e·s des universités françaises dispensant des formations à distance à un public empêché. Il est illustré d’expériences conduites par d’autres universités ainsi que des outils et méthodes innovants développés par des ingénieur·e·s pédagogiques de l’Université Paul-Valéry Montpellier 3 dans le cadre du projet.
## Guide ethnographique
[Accéder à la ressource](https://idefi-livret3-webdoc.univ-montp3.fr)

Public cible :
- Enseignants, formateurs, ingénieurs pédagogiques et médiateurs numériques
- Animateurs et porteurs de projets
- Chercheurs, doctorants, masterants intéressés par ce terrain

Description :
Ce guide interactif retrace l'expérience des acteurs de ce projet, sous forme d'un webdocumentaire où les vidéos se succèdent. Au travers des vidéos, vous pouvez découvrir l’environnement concret des étudiants détenus, les enjeux et les contraintes des formations à distance, ainsi que les réponses des différents acteurs mobilisés sur le terrain.