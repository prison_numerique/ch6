---
title: "#3  Expérience"
draft: false
tags:
---
Pour commencer, cette expérience a renforcé l'idée qu'il n'y a pas de dispositif numérique **sans connaissance préalable et régulièrement actualisée** de ce qu'il se passe sur le terrain. 

Ensuite, il est plus que nécessaire de valoriser l'apprentissage en prison en passant par des modules de formations numériques courtes qui mobilisent des savoirs de culture générale et des littératies numériques. 

Par ailleurs, et dans un tel contexte, l'expérience montre l'implication médiatrice de l'enseignant, formateur, éducateur, RLE en prison. La médiation humaine que cet acteur peut prendre en charge est essentielle pour développer ce type de projet et de méthodologie par exemple par la reformulation et les pratiques pluri-sémiotiques qui se caractérisent par l'utilisation simultanée de divers modes sémiotiques tels que les gestes, la parole, les schémas tout en intégrant différents modes de communication pour produire des effets pragmatiques sur l'apprentissage ([Rabatel 2011](https://www.cairn.info/revue-litterature-2011-3-page-108.htm)) . C'est ainsi que les modules de cours numériques ont été produits pour être attrayants et esthétiques et ainsi être mieux consultés, conçus aussi pour pouvoir échanger entre pairs à partir d'un exercice ou encore, pour introduire des temps de reformulation pris en charge par l'enseignant ou le bénévole. 

Un autre point consiste à encourager à travers cette méthodologie la conception d'exercices en auto-apprentissage et en collaboration sur la base par exemple de consigne d'écriture d'un texte à quatre mains. En guise de perspective, cette expérience de projet a montré que trois dispositifs numériques pouvaient être favorisés : le récit audio, le portfolio numérique ou encore le livre numérique augmenté.