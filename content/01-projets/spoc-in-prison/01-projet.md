---
title: "#1  Projet"
draft: false
tags:
---
## Contexte
Pour situer le projet "SPOC in prison", et donc envisager les moyens qui ont été mis en œuvre pour ce projet international porté entre 2018 et 2021, nous proposons quelques éléments de contexte.
### Un programme ERASMUS+
Le projet "SPOC in prison" s'inscrit dans le cadre du programme ERASMUS+, spécifiquement dans la catégorie des échanges de bonnes pratiques pour l’éducation des adultes (Action Clé 2). [Ce programme](https://erasmus-plus.ec.europa.eu/projects/search/details/2018-1-FR01-KA204-047927), financé par la Commission Européenne, vise à soutenir l'éducation, la formation, la jeunesse et le sport en Europe, promouvant ainsi l'échange et l'innovation à travers différents types d'actions éducatives.
### Notre projet ERASMUS+
Le projet a été mené par un consortium composé de quatre organisations européennes ayant une expertise dans l'enseignement numérique et/ou dans le contexte pénitentiaire. Les principaux acteurs étaient l'Institut des Technosciences de l'Information et de la Communication (ITIC) de l'Université Paul-Valéry Montpellier 3, un centre pour la promotion de l'apprentissage tout au long de la vie, l'association Antigone Onlus et la Prison italienne de Benevento. L'équipe a également collaboré avec Mme Véronique Béguet de la Chaire UNESCO de recherche appliquée pour l’éducation en prison au Québec, apportant une dimension internationale significative au projet.

L'objectif principal du projet "SPOC in prison" était de développer l'utilisation des outils numériques par les personnes détenues et leurs formateurs, afin de favoriser une éducation adaptée et accessible. Le projet s'est déroulé en plusieurs phases :
1. Une phase de collecte d'informations sur les pratiques numériques existantes en milieu carcéral.
2. Une phase de production de contenu pédagogique numérique adapté aux besoins spécifiques de l'environnement carcéral.
3. Une phase de test de ces modules numériques à la prison de Benevento, comprenant des formations pour les formateurs et une évaluation approfondie de l'expérience.
Ces activités ont permis non seulement de tester de nouvelles pratiques pédagogiques mais aussi de créer des ressources numériques spécifiques qui ont été largement partagées et discutées au sein du consortium, enrichissant ainsi l'expertise collective sur l'éducation numérique en prison.

<iframe width="560" height="315" src="https://www.youtube.com/embed/maQqpQP_6_g?si=zNCiPVAfZ0tv2Bwb" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Le projet a ainsi offert une opportunité unique de réfléchir à la manière dont les technologies numériques peuvent être adaptées pour répondre aux défis de l'éducation en milieu carcéral, tout en favorisant une meilleure compréhension des besoins et des possibilités d'apprentissage des détenus.
## Collaborateurs
Le programme Européen Erasmus+ multiplie les dispositifs de financement notamment pour valoriser les actions de formation tout au long de la vie. C'est ainsi que de nombreux projets (consultables sur la plateforme Epale) se rapportent à l'éducation en prison. Parmi ceux-ci le projet SPOCinprison compte 4 collaborateurs clés :  
1. Le détenu en situation d'apprentissage qui, quelque soit le niveau et la formation concernée, reprend un processus de scolarisation ou d'études et doit s'employer à consulter, rédiger et passer des examens dans une temporalité à deux vitesses : celle de la formation et celle de la détention que l'on sait rarement compatible.  
2. L'enseignant ou formateur ou éducateur ou RLE (selon les pays partenaires du projet) a accompagné les détenus dans la prise en main des ressources, dans une médiation aussi lorsque le contenu didactique le permettait. C'est aussi ce collaborateur qui appuie la demande du porteur du projet auprès du directeur d'établissement pénitentiaire.  
3. Le directeur-rice de prison autorise les entrées de matériel et l'aménagement de l'emploi du temps de la détention.  
4. Le responsable informatique prend en charge les ressources via envoie par courriel ou lien de téléchargement pour ensuite procéder à l'installation sur les machines informatiques.

## Périmètre
#### Publics cibles dans le projet "SPOC in prison"
Ce projet visait spécifiquement les personnes détenues et leurs formateurs, avec un accent sur la création et la mise en œuvre de modules de formation numérique qui peuvent fonctionner de manière autonome sans accès à Internet. Ces ressources devaient être intégrées de manière à permettre une utilisation fluide et efficace, même dans un contexte aussi restrictif que celui des établissements pénitentiaires.
### Objectif du projet
L'initiative visait à transformer l'accès à l'éducation dans les prisons en intégrant des outils numériques modernes, en formant les formateurs à ces technologies, et en développant des contenus pédagogiques spécifiquement adaptés à ce contexte.
### Moyens
Pour réaliser ces ambitions, le projet a bénéficié d'un financement européen, appuyé par une collaboration interinstitutionnelle forte entre les différents partenaires du consortium, chacun apportant son expertise et ses ressources pour la réussite du projet.
