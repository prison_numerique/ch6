---
title: "#2  Ressources"
draft: false
tags:
---
## Ressources du projet "SPOC in Prison"

Les résultats et livrables du projet "SPOC in Prison" étaient centrés sur le développement et l'implémentation de solutions pédagogiques numériques adaptées à l'environnement carcéral. Voici une liste détaillée des principales ressources produites durant le projet :

1. **État de l'art de l'éducation numérique en prison** : _Ce document offre un aperçu complet des pratiques pédagogiques numériques actuelles dans les environnements carcéraux._
2. **Base de données de projets européens** : _Recense les initiatives proposant des formations digitales en milieu pénitentiaire._
3. **Kit méthodologique et pédagogique** : _Introduction à l'ingénierie numérique spécifique aux besoins des formateurs en milieu carcéral._
4. **Modèles de parcours de formation numérique** : _Ces modules sont adaptés spécifiquement pour l'usage en prison, couvrant divers sujets éducatifs._

### État de l'art de l'éducation numérique en prison

[Accéder à la ressource](media/spoc_in_prison_etat_de_l_art_sept2020.pdf) (Versions : [[spoc_in_prison_state_of_the_art_sept2020.pdf|EN]] - [[spoc_in_prison_stato_dell_arte_sept2020.pdf|IT]])

**Public cible :**

- Chercheurs et praticiens de l'éducation en prison
- Responsables de programmes éducatifs en milieu carcéral
- Décideurs politiques intéressés par l'amélioration des pratiques éducatives en prison

**Description :** Ce document résume les connaissances actuelles et les pratiques autour de l'utilisation des technologies numériques dans l'éducation carcérale, offrant un panorama des méthodes et des outils utilisés pour faciliter l'apprentissage dans ces conditions spécifiques.

### Base de données de projets européens

[[spoc_in_prison_bdd_projets_eu.pdf|Accéder à la ressource]]

**Public cible :**

- Coordinateurs de projet
- Institutions éducatives
- Organismes gouvernementaux

**Description :** Cette base de données compile divers projets et initiatives à travers l'Europe qui implémentent des formations numériques en milieu pénitentiaire, fournissant une ressource précieuse pour ceux qui cherchent à comprendre les tendances et les stratégies en cours.

### Kit méthodologique et pédagogique

[[media/opale/Module_of_Introduction_to_digital_engineering/co/module.html|Accéder à la ressource]]

**Public cible :**

- Formateurs et enseignants en milieu carcéral
- Ingénieurs pédagogiques
- Administrateurs de programmes éducatifs

**Description :** Ce kit fournit les outils et les techniques nécessaires pour la création et la mise en œuvre de modules de formation numérique adaptés aux restrictions et aux besoins spécifiques de l'environnement carcéral.

### Modèles de parcours de formation numérique

**Modules disponibles :**
Proposés en guise d'entrainement
- 🇬🇧 <a traget="_blank" href="https://prison_numerique.gitpages.huma-num.fr/ch6/media/opale/humanum">Humanum introduction aux humanités digitales</a>
- 🇬🇧 <a traget="_blank" href="https://prison_numerique.gitpages.huma-num.fr/ch6/media/opale/Greta">Greta Thunberg</a>
- 🇬🇧 [Lien entre langage et numérique](https://prison_numerique.gitpages.huma-num.fr/ch6/media/opale/digit_and_language)
- 🇬🇧 [Le petit Prince](https://prison_numerique.gitpages.huma-num.fr/ch6/media/opale/Le_petit_prince)

Proposés par nos partenaires
- 🇮🇹 [Non solo sbarre... Ma anche emozioni](https://prison_numerique.gitpages.huma-num.fr/ch6/media/opale/emotions)

**Public cible :**
- Détenus : les modules sont conçus à leur attention.
- Formateurs en milieu carcéral : en guise d'exemple.

**Description :** Ces modules offrent une variété de contenus éducatifs adaptés pour l'utilisation en milieu carcéral, permettant aux détenus d'explorer différents domaines du savoir et de développer leurs compétences en utilisant des outils numériques spécialement conçus pour cet environnement.

Ces ressources sont le fruit d'une collaboration intensive et d'une recherche approfondie, visant à améliorer significativement les opportunités d'éducation pour les personnes détenues et à enrichir les méthodes d'enseignement en milieu carcéral.