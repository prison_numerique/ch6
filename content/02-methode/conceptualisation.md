---
title: "#2 Conceptualisation"
draft: false
tags:
---
La conceptualisation d'une médiation pédagogique numérique en milieu carcéral nécessite une approche méthodique qui aligne étroitement les besoins éducatifs identifiés, les contraintes spécifiques de l'environnement carcéral, et les opportunités offertes par la technologie. Voici un guide pour conceptualiser efficacement ce type de projet :

## Étape 1 : Définir les Objectifs Pédagogiques

Basés sur les besoins identifiés dans le rapport de besoins, définissez des objectifs pédagogiques clairs et mesurables. Ces objectifs doivent répondre directement aux lacunes ou aux opportunités éducatives relevées lors de l'analyse des besoins. Ils doivent également être adaptés au contexte spécifique de l'apprentissage en prison, en tenant compte des restrictions de temps, d'espace et d'accès à la technologie.

> [!NOTE] On entre dans l'ingénierie pédagogique
> La phase de définition des objectifs pédagogiques est la première étape à tout projet pédagogique conçu dans une perspective d'ingénierie pédagogique.
> Ces objectifs doivent s'appuyer à la fois sur les modalités de contrôle de connaissance (évaluations) et sur les besoins spécifiques.
>  
> **Dans le cadre universitaire :**
> Les modalités de contrôle de connaissance sont à explorer avec le responsable pédagogique de l'unité d'enseignement, en accord avec la direction de la scolarité.
>
> **Hors cadre universitaire :**
> Le cadre est moins rigide, mais les compétences à valider doivent sensiblement rester les mêmes. Pour autant, des objectifs connexes ou complémentaires peuvent être formulés. Notamment en matière de littératies numériques.

## Étape 2 : Choisir les technologies appropriées

Pour cela, reportez-vous aux conditions d'accès à la technologie présentés dans le deuxième chapitre, et à la revue technopédagogique proposée en média d'enrichissement.

Adaptez les technologies employées aux possibilités offertes par les établissements, et prenez en compte la compétence des enseignants impliqués.

## Étape 3 : Concevoir le Contenu Pédagogique

Développez des contenus qui sont engagés et adaptés aux apprenants en milieu carcéral. Cela comprend la création de matériel pédagogique qui peut être facilement compris et retenu, des activités interactives, et des évaluations qui respectent les capacités et les contraintes des apprenants. Il est crucial que le contenu soit culturellement pertinent et conçu pour renforcer les compétences nécessaires à une réintégration sociale réussie.

## Étape 4 : Développer des Stratégies d'Engagement des Apprenants

Étant donné les défis uniques liés à la motivation des détenus à participer activement à l'apprentissage, développez des stratégies spécifiques pour engager les apprenants. Cela pourrait inclure des incitations basées sur les accomplissements dans les modules d'apprentissage, des approches de gamification, ou des projets de groupe qui encouragent la collaboration et le soutien entre pairs.


> [!NOTE] Environnement carcéral et engagement
> Quand on parle d'engagement, on invoque plusieurs concepts :
> - Le maintien de l'attention,
> - La motivation à l'apprentissage,
> - La capacité de l'individu à recevoir la connaissance ou s'impliquer dans les exercices,
> - L'intérêt de l'individu pour les sujets abordés
> Par chance, nous avons plusieurs outils qui permettent de s'engager dans un cours malgré les mauvaises conditions d'apprentissage. Et la plupart ne s'appuient pas sur l'outil ou la technologie employée.
> **Attentionnelle :** Intéressez-vous à la charge cognitive de Sweller et les principes de Mayers pour concevoir des supports multimédia engageants et accessibles.
> **Motivation :** Sollicitez des motivations dites "autodéterminées", en appelant l'apprenant à lier le programme avec ses aspirations et ses centre d'intérêt.
> **Accessibilité :** Adoptez un langage adapté, proposez des lexiques et des récapitulatifs. Ils sont particulièrement appréciés par les apprenants qui ont un faible niveau de lecture.
> **Exercices :** Proposez régulièrement des exercices de compréhension. S'ils sont fréquents, concevez-les pour qu'ils soient rapides et faciles. S'ils sont plus rares, engagez l'apprenant à travailler depuis sa cellule entre deux sessions.

## Étape 5 : Planifier la Formation des Éducateurs

Les éducateurs jouent un rôle crucial dans la réussite de tout programme éducatif en milieu carcéral. Planifiez des sessions de formation pour les éducateurs sur l'utilisation des technologies choisies, les meilleures pratiques pédagogiques dans un environnement carcéral, et les techniques pour gérer efficacement les défis comportementaux et émotionnels des apprenants.


> [!NOTE] Supports dédiés aux éducateurs
> Si votre formation à destination des éducateurs, tuteurs et enseignants est synchrone, proposez de la documentation complémentaire, ou des modules d'apprentissage numérique.

## Étape 6 : Tester et Valider le Concept

Avant le déploiement complet, conduisez une phase pilote pour tester le concept de la médiation pédagogique numérique avec un petit groupe de détenus. Utilisez cette phase pour recueillir des retours, ajuster les méthodes d'enseignement, le contenu, et la technologie selon les besoins.

## Étape 7 : Évaluer et Ajuster

Mettez en place un système robuste d'évaluation pour mesurer l'efficacité du programme. Cela devrait inclure des feedbacks réguliers des apprenants et des éducateurs, ainsi que des évaluations formelles des progrès réalisés par rapport aux objectifs pédagogiques. Utilisez ces informations pour faire des ajustements continus au programme afin de maximiser son impact.

## Étape 8 : Documentation et Rapports

Documentez tout le processus de conceptualisation, de mise en œuvre, d'évaluation et d'ajustement. Ces documents peuvent aider à informer les futurs efforts de médiation numérique dans le même établissement ou servir de modèle pour d'autres initiatives similaires.

En suivant ces étapes pour la conceptualisation de votre projet de médiation pédagogique numérique, vous serez mieux préparé pour répondre efficacement aux besoins éducatifs des détenus tout en respectant les contraintes du milieu carcéral.