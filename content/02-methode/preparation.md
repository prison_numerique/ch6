---
title: "#0 Préparation administrative"
draft: false
tags:
---
## La sécurité en prison
Bien sûr, lorsque un médiateur numérique entreprend de réaliser une ethnographie visuelle dans un établissement pénitentiaire, il doit naviguer à travers plusieurs couches de protocoles de sécurité rigoureux.

D'abord, il doit obtenir des autorisations appropriées avant même de pouvoir commencer son projet. Cela signifie soumettre une proposition détaillée à la direction de l'établissement et souvent aussi à une autorité centrale, comme le ministère de la Justice. Cette proposition doit expliquer clairement les objectifs du projet, les méthodes, le type de matériel de captation utilisé, et comment il prévoit de gérer et sécuriser les données recueillies.

Une fois sur place, le médiateur doit se conformer aux règles strictes de l'établissement concernant l'introduction et l'utilisation de matériel électronique. Dans beaucoup de prisons, les appareils tels que les caméras, les enregistreurs audio et les smartphones sont soit fortement réglementés, soit totalement interdits à l'intérieur des zones sécurisées. Le médiateur pourrait avoir besoin d'utiliser du matériel approuvé par l'établissement ou de faire en sorte que son matériel soit vérifié et scellé par la sécurité de la prison.

Certaines zones géographiques de la prison ne pourront pas être capturées par la caméra. Par ailleurs, il est interdit, pour des raisons de sécurité, de sortir de l'enceinte de l'établissement des images qui permettent de se repérer au sein de l'établissement. Certains établissements refusent de filmer les fenêtres, ou les cour extérieures.

Le médiateur doit également être préparé à travailler sous surveillance constante du personnel de la prison et à respecter toutes les directives données par ce personnel pour assurer sa sécurité et celle des détenus. Cela inclut potentiellement des restrictions sur les zones accessibles, les horaires pendant lesquels il peut travailler, et les interactions avec les détenus.

Enfin, en ce qui concerne la gestion des données, le médiateur doit garantir que toutes les informations collectées seront traitées avec le plus haut niveau de confidentialité et de sécurité. Cela implique souvent de crypter les données visuelles et de s'assurer que seul le personnel autorisé a accès à ces informations. De plus, toutes les images et vidéos capturées doivent être traitées de manière à respecter la vie privée des détenus, en anonymisant (flou) les visages ou en évitant de capturer des détails identifiables, sauf si un consentement explicite a été donné.

Tous ces protocoles sont essentiels non seulement pour la sécurité et la conformité réglementaire mais aussi pour maintenir la confiance entre les participants au projet, les autorités pénitentiaires et le médiateur numérique lui-même.

<hr>

## Préparation administrative
Pour mener une étude ethnographique dans un milieu carcéral, en particulier pour développer ou proposer une médiation numérique en éducation, la préparation administrative est un élément crucial qui nécessite de la minutie et une bonne compréhension des procédures de sécurité et des politiques de confidentialité en vigueur dans les institutions pénitentiaires. Voici les étapes essentielles pour préparer votre venue dans une prison.

> [!warning] Vigilance
> **Certaines étapes vous paraissant lointaines et sont pourtant citées dès maintenant**, et ce parce que comme tout terrain sensible, vous devez tout baliser avant de commencer votre démarche. Les autorisations sont longues à recueillir, et vous devez montrer "patte blanche".

## Étape 1 : Définition du Projet

- **Objectifs Clairs** : Définissez clairement les objectifs de votre étude ethnographique, y compris le focus sur la médiation numérique.
- **Plan de Recherche** : Élaborez un plan de recherche détaillé, y compris les méthodes d'observation, les types d'interaction avec les détenus, et les outils numériques envisagés.

## Étape 2 : Prise de Contact avec les Autorités Pénitentiaires

- **Identification des Contacts** : Identifiez les personnes responsables dans l'administration pénitentiaire, souvent le directeur de l'établissement ou le responsable de la formation et de l'éducation.
- **Lettre de Demande** : Rédigez une lettre de demande d'autorisation pour mener une recherche ethnographique. Incluez dans cette lettre :
    - Une présentation de vous-même et de votre affiliation institutionnelle.
    - Une description concise de l'étude, ses objectifs, et comment elle peut bénéficier à l'établissement.
    - Les détails spécifiques des activités prévues et des technologies que vous souhaitez utiliser.

## Étape 3 : Autorisations Nécessaires

- **Demande d'Autorisation Officielle** : Vous devrez obtenir une autorisation officielle de la part de la direction de l'établissement pénitentiaire, et potentiellement de l'autorité centrale de gestion des prisons (par exemple, le ministère de la Justice ou l'administration pénitentiaire nationale).
- **Protocoles de Sécurité** : Assurez-vous de comprendre et de respecter les protocoles de sécurité spécifiques à l'établissement. Cela peut inclure des règles sur l'utilisation des équipements électroniques, la gestion des données sensibles, etc.

## Étape 4 : Éthique et Confidentialité

- **Consentement Éclairé** : Préparez des formulaires de consentement éclairé pour tous les participants, clarifiant comment les données seront utilisées, stockées et protégées.
- **Respect de la Confidentialité** : Mettez en place des mesures pour garantir la confidentialité et la sécurité des informations recueillies pendant l'étude.

### Étape 5 : Logistique sur Place

- **Visites Préliminaires** : Si possible, organisez des visites préliminaires pour familiariser avec l'environnement et affiner votre approche en fonction des conditions sur le terrain.
- **Coordination avec le Personnel** : Établissez une bonne communication avec le personnel pénitentiaire qui peut aider à faciliter les interactions avec les détenus et assurer le bon déroulement du projet.

### Étape 6 : Documentation Supplémentaire

- **Proposition de Recherche Détaillée** : En plus de la lettre, vous pourriez devoir soumettre une proposition de recherche détaillée aux comités d'éthique institutionnels ou aux autorités pénitentiaires.
- **Assurances** : Vérifiez si des assurances spécifiques sont nécessaires pour mener des recherches dans un contexte carcéral.

### Étape 7 : Suivi Post-Étude

- **Rapports de Progrès et Finaux** : Prévoyez de soumettre des rapports intermédiaires et finaux aux autorités pénitentiaires, partageant les découvertes et recommandations de l'étude.

Préparer minutieusement ces étapes avant de commencer la recherche assure non seulement la conformité avec les réglementations mais aussi le respect et la sécurité de toutes les parties impliquées.

<hr> 

## Ressources
- [Lettre 1](media/lettre_1.pdf)
- [Lettre 2](media/lettre_2.pdf)
- [Courriel 1](media/courriel_1.pdf)
- [Courriel 2](media/courriel_2.pdf)
