---
title: "#3 Scénarisation"
draft: false
tags:
---
Vous avez normalement constitué votre cahier des charges, et conceptualisé les contenus de votre formation. Vous savez quelles technologies employer, et vous maîtrisez parfaitement les conditions dans lesquelles les apprenants s'attelleront à leur tâche d'apprentissage. Passons maintenant à la scénarisation.

La scénarisation pédagogique est l'étape qui permet de passer du concept de la formation à la mise en forme structurée et organisée de votre contenu éducatif en modules ou leçons interactives, en utilisant efficacement les technologies choisies. Cette étape transforme votre concept en un parcours d'apprentissage concret et engageant pour les apprenants.
### Étape 1 : Définition de la Structure du Cours
Sur la base de la conceptualisation, organisez le contenu en modules clairement définis. Chaque module doit répondre à des objectifs spécifiques, s'inscrivant dans l'objectif global du cours. La structure doit considérer les limitations temporelles et d'accès typiques à l'environnement carcéral.
### Étape 2 : Conception du Scénario Pédagogique
Traduisez les concepts en activités d'apprentissage spécifiques. Ce "scénario pédagogique" ([[#Modèle de scénario pédagogique|modèle proposé ci-après]]) inclura les instructions détaillées pour chaque partie du module, les interactions prévues, et les transitions entre les sections. Pensez à comment chaque élément du cours aidera les détenus à atteindre les objectifs d'apprentissage.
### Étape 3 : Sélection et Intégration Multimédia
Identifiez les supports multimédias qui renforceront le scénario pédagogique. Utilisez des graphiques, des vidéos, et des animations pour illustrer les points clés. Assurez-vous que ces ressources soient non seulement informatives mais aussi conformes aux restrictions technologiques et sécuritaires de l'environnement pénitentiaire.
### Étape 4 : Interactivité et Engagement
Concevez des éléments interactifs à intégrer dans chaque module pour maintenir l'engagement des apprenants. Utilisez des quiz, des simulations, et des forums de discussion adaptés aux plateformes autorisées, pour rendre l'apprentissage dynamique et participatif.
### Étape 5 : Test, Révision et Feedback
Testez le cours dans son environnement prévu pour identifier les problèmes techniques et pédagogiques. Impliquez un petit groupe de détenus et de personnel pénitentiaire pour obtenir des retours qui vous aideront à affiner le scénario pédagogique.
### Étape 6 : Accessibilité et Adaptation
Assurez-vous que le contenu est accessible à tous les apprenants, y compris ceux avec des besoins spécifiques. Adaptez les supports pour qu'ils soient clairs et compréhensibles, en intégrant des aides visuelles et auditives si nécessaire.
### Étape 7 : Sécurité et Confidentialité
Vérifiez que toutes les composantes du cours respectent les normes de sécurité élevées requises en milieu carcéral. Cela comprend la protection des données personnelles des étudiants et la prévention de l'accès non autorisé aux contenus.
### Étape 8 : Documentation et amélioration continue
Documentez le processus de scénarisation, les choix faits, les problèmes rencontrés, et comment ils ont été adressés. Évaluez régulièrement le cours avec des feedbacks des utilisateurs pour l'améliorer continuellement.

<hr>

## Modèle de scénario pédagogique
À compléter.

| Étape du Cours | Objectifs d'Apprentissage                                                                              | Contenu                                                         | Activités                                                            | Ressources Multimédia                           | Méthodes d'Évaluation                | Notes de Sécurité                                                                  |
| -------------- | ------------------------------------------------------------------------------------------------------ | --------------------------------------------------------------- | -------------------------------------------------------------------- | ----------------------------------------------- | ------------------------------------ | ---------------------------------------------------------------------------------- |
|                |                                                                                                        |                                                                 |                                                                      |                                                 |                                      |                                                                                    |
