---
title: Guide du porteur de projet en milieu carcéral
draft: false
tags: 
date: 05-03-2024
---
Ce média hypertextuel vous est proposé comme conclusion à l'ouvrage "Prison à l'ère numérique". Il relie et convoque tous les enseignements tirés des expérimentations de terrain menées en matière de médiations numériques sur le terrain carcéral.

## Fin du récit
Nous vous recommandons d'écouter le récit 5 avant de parcourir ce guide.
## Comment fonctionne ce guide hypertextuel ?
Dans l'explorateur, vous pouvez consulter les [[01-projets/index|projets]] qui ont été menés. Chacun vous est présenté avec son contexte de réalisation, les ressources qui ont été produites à cet effet, et, le plus précieux, un retour sur expérience.

Si vous consultez ce média dans l'ordre recommandé par l'ouvrage, vous vous apprêtez à lire l'expérience de L. Alidières sur une première mise en place d'une médiation numérique pédagogique en prison, au cours de sa thèse, entrepris au cours de son mémoire de master.

Cette première mise en place est la plus documentée d'entre toutes. Nous vous en recommandons la lecture attentive, mais vous offrons ce [[02-methode/index|guide méthodologique]] en guise d'outil de travail pour vos prochaines réalisation.

