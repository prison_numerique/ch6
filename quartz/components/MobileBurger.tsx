import { QuartzComponent, QuartzComponentConstructor, QuartzComponentProps } from "./types"
import mobileBurgerStyle from "./styles/mobileBurger.scss"

// This import is only necessary if TypeScript doesn't complain about missing types.
// Otherwise, it can be omitted since scripts are not typically imported in TypeScript
// for execution but referenced in HTML or loaded externally.
// @ts-ignore
import mobileBurgerScript from "./scripts/mobileBurger.inline"

interface MobileBurgerOptions {
  displayClass?: "mobile-only" | "desktop-only"
}

const defaultOptions: MobileBurgerOptions = {
  displayClass: "mobile-only"
}

export default ((Component?: QuartzComponent, userOpts?: MobileBurgerOptions) => {
  const opts = { ...defaultOptions, ...userOpts };

  const MobileBurger: QuartzComponent = (props: QuartzComponentProps) => {
    // Component rendering here
    return (
      <div className={opts.displayClass}>
        <div id="burger-icon" className="burger-icon">
          <div></div>
          <div></div>
          <div></div>
        </div>
        <div id="fullscreen-menu" className="fullscreen-menu">
          {Component ? <Component {...props} /> : <p>No component provided</p>}
        </div>
      </div>
    );
  };

  // Styles and scripts for the component
  MobileBurger.css = mobileBurgerStyle;
  MobileBurger.afterDOMLoaded = mobileBurgerScript;

  return MobileBurger;
}) satisfies QuartzComponentConstructor;
