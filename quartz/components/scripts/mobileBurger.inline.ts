document.addEventListener('DOMContentLoaded', () => {
    const burgerIcon = document.getElementById('burger-icon');
    const fullscreenMenu = document.getElementById('fullscreen-menu');
  
    if (burgerIcon && fullscreenMenu) {
      const toggleMenu = () => {
        fullscreenMenu.classList.toggle('open');
        if (fullscreenMenu.classList.contains('open')) {
          fullscreenMenu.style.transform = 'translateX(0)';
        } else {
          fullscreenMenu.style.transform = 'translateX(-100%)';
        }
      };
  
      const handleDocumentClick = (event: MouseEvent) => {
        if (event.target instanceof Node) {
          if (!burgerIcon.contains(event.target) && !fullscreenMenu.contains(event.target)) {
            if (fullscreenMenu.classList.contains('open')) {
              fullscreenMenu.classList.remove('open');
              fullscreenMenu.style.transform = 'translateX(-100%)';
            }
          }
        }
      };
  
      burgerIcon.addEventListener('click', toggleMenu);
      document.addEventListener('click', handleDocumentClick);
  
      // Gestion du nettoyage pour éviter les fuites de mémoire
      window.addCleanup(() => {
        burgerIcon.removeEventListener('click', toggleMenu);
        document.removeEventListener('click', handleDocumentClick);
      });
    }
  });
  